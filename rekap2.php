<?php

    require_once("config.php");
    $sql_get = "SELECT * FROM pembeli";
$query_pembeli = mysqli_query($koneksi, $sql_get);
$results = [];
while($row = mysqli_fetch_assoc($query_pembeli)) {
    $results[] = $row;
}
?>


<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>TOKO CAT ADIGUNA</title>
    </head>
    <body>
      <div class="content">
            <h1 class="judul"> TOKO CAT ADIGUNA</h1>
        
        <div class="menu">
            <ul>
                <li><a href="index.php">HOME</a></li>
                <li><a href="tentang.html">TENTANG ADIGUNA</a></li>
                <li class="active"><a href="rekap.php">REKAP DATA/STOCK</a></li>
                <li><a href="rekap2.php">REKAP DATA KELUAR</a></li>
                <li><a href="login.html">LOG OUT</a></li>

            </ul>
        </div>
    
        <!-- Tampilan awal-->
        <div class="rekap">
        <center>
        <table border="3" width="80%">
        <tr>
            <td>no</td>
            <td>Id pembeli</td>
            <td>Nama Pembeli </td>
            <td>Alamat</td>
            <td>No_hp</td>
            <td>Jumlah barang</td>
            <td>kode_produk</td>
            <td>
            <a href="tambah2.php">create Data</a>
          
            </td>
            <br>
        </tr>
        <?php
        $no = 1;
        foreach ($results as $result) :

        ?>

        <tr>
            
            <td> <?= $no; ?> </td>
            <td> <?= $result ['id_pembeli'] ?> </td>
            <td> <?= $result ['nama'] ?> </td>
            <td> <?= $result ['alamat'] ?> </td>
            <td> <?= $result ['no_hp'] ?> </td>
            <td> <?= $result ['jumlah_barang'] ?> </td>
            <td> <?= $result ['kode_produk'] ?> </td>
            <td>
                <a href="edit2.php?id_pembeli=<?=$result['id_pembeli'];?>">Update</a>
                ||
                <a href="hapus2.php?id_pembeli=<?=$result['id_pembeli'];?>">Delete</a>
            </td>

        </tr>

        <?php 
        $no++;
        endforeach;
         ?>
    </table>

        
    
        <!-- bagian copyright-->
    <header>
        <div id="container">
            <div id="header">
                <h4>Copyright @1900018110 </h4>
                <h4>Tatang juliyanto</h4>
        </div>
        
    </header>
    
    </div>
    </body>
    </html>
    